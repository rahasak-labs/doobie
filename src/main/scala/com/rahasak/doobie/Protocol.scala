package com.rahasak.doobie

case class Document(id: String, name: String, timestamp: Long)

case class User(id: String, name: String)

