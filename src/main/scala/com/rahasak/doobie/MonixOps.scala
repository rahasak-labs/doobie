package com.rahasak.doobie

import monix.eval.Task

import scala.concurrent.duration._
import scala.util.{Failure, Random, Success}

object MonixOps extends App {
  def getCount(): Task[Int] = {
    Task {
      // sleep for while
      Thread.sleep(2000)

      val i = Random.nextInt(10)
      println(s"got $i")

      i
    }
  }

  def makeText(i: Int): Task[String] = {
    Task {
      // sleep for while
      Thread.sleep(2000)

      val t = "monix " * i
      println(s"got $t")

      t
    }.timeout(3.seconds)
  }

  // scala ExecutionContext has been replaced with a Monix global Scheduler
  // Tasks are not started when they created. Therefore no Scheduler is needed before we start/run the task
  import monix.execution.Scheduler.Implicits.global

  // run tasks with blocking
  val task1 = getCount()
  val task2 = makeText(4)
  task1.runSyncUnsafe()
  task2.runSyncUnsafe()

  // combine with for compression
  val task3 = for {
    i <- getCount()
    t <- makeText(i)
  } yield t

  // run task with non blocking
  // runToFuture returns cancellable future
  val future = task3.runToFuture
  future.onComplete {
    case Success(value) => println(s"done future with result - $value")
    case Failure(e) => e.printStackTrace()
  }

  // wait forever, otherwise app will exists before future finish
  while (true) {}
}

