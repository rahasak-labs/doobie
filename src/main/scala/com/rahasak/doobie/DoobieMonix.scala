package com.rahasak.doobie

import com.zaxxer.hikari.{HikariConfig, HikariDataSource}
import doobie.hikari.HikariTransactor
import doobie.implicits._
import monix.eval.Task

import scala.concurrent.Future
import scala.util.{Failure, Success}

object DoobieMonix extends App {

  // hikari pooling config with mysql
  val config = new HikariConfig()
  config.setJdbcUrl("jdbc:mysql://localhost:3306/mystiko")
  config.setUsername("root")
  config.setPassword("root")
  config.setMaximumPoolSize(5)

  // transactor with Monix Task
  val transactor: Task[HikariTransactor[Task]] =
    Task(HikariTransactor.apply[Task](new HikariDataSource(config)))

  // scala ExecutionContext has been replaced with a Monix global Scheduler
  // Tasks are not started when they created. Therefore no Scheduler is needed before we start/run the task
  import monix.execution.Scheduler.Implicits.global

  // create table
  val c = for {
    xa <- transactor
    result <- Query.createTable.run.transact(xa)
  } yield result
  println(c.runSyncUnsafe()) // runSyncUnsafe is blocking
  val future: Future[Int] = c.runToFuture // runToFuture is non blocking with future
  future.onComplete {
    case Success(value) => println(s"done future with result - $value")
    case Failure(e) => e.printStackTrace()
  }

  // insert
  val i1 = for {
    xa <- transactor
    result <- Query.insert(Document("002", "rahasak", System.currentTimeMillis() / 100)).run.transact(xa)
  } yield result
  println(i1.runSyncUnsafe())

  // insert
  val i2 = for {
    xa <- transactor
    result <- Query.insert(Document("001", "rahasak", System.currentTimeMillis() / 100)).run.transact(xa)
  } yield result
  println(i2.runSyncUnsafe())

  // search
  val s1 = for {
    xa <- transactor
    result <- Query.search("rahasak").to[List].transact(xa)
  } yield result
  s1.runSyncUnsafe().foreach(println)

  // search with id
  val s2 = for {
    xa <- transactor
    result <- Query.searchWithId("001").option.transact(xa)
  } yield result
  println(s2.runSyncUnsafe())

  // search with fragment
  val s3 = for {
    xa <- transactor
    result <- Query.searchWithFragment("rahasak", asc = true).to[List].transact(xa)
  } yield result
  s3.runSyncUnsafe().foreach(println)

  // update
  val u = for {
    xa <- transactor
    result <- Query.update("001", "rahasak-labs").run.transact(xa)
  } yield result
  println(u.runSyncUnsafe())

  // delete
  val d = for {
    xa <- transactor
    result <- Query.delete("001").run.transact(xa)
  } yield result
  println(d.runSyncUnsafe())

}

