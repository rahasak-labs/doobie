package com.rahasak.doobie

import cats.effect.IO

import scala.util.{Failure, Random, Success}

object CatsOps extends App {
  def getCount(): IO[Int] = {
    IO {
      // sleep for while
      Thread.sleep(2000)

      val i = Random.nextInt(10)
      println(s"got $i")

      i
    }
  }

  def makeText(i: Int): IO[String] = {
    IO {
      // sleep for while
      Thread.sleep(2000)

      val t = "monix " * i
      println(s"got $t")

      t
    }
  }

  // run IO with blocking
  val task1 = getCount()
  val task2 = makeText(4)
  task1.unsafeRunSync()
  task2.unsafeRunSync()

  // combine with for compression
  val io = for {
    i <- getCount()
    t <- makeText(i)
  } yield t

  // run IO with non blocking
  // unsafeToFuture returns future
  implicit val ec = scala.concurrent.ExecutionContext.Implicits.global
  val future = io.unsafeToFuture()
  future.onComplete {
    case Success(value) => println(s"done future with result - $value")
    case Failure(e) => e.printStackTrace()
  }

  // wait forever, otherwise app will exists before future finish
  while (true) {}
}
